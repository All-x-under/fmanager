package ua.javaF1;

import javax.swing.JFrame;

public class Main {

	public static void main(String[] args) {
		JFrame f = new JFrame("Окно игры F1");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(550, 600);
		
		f.add(new Track());
		f.setVisible(true);
		
		JFrame f2 = new JFrame("Окно игры F2");
		f2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f2.setSize(550, 600);
		
		f2.add(new Track());
		f2.setVisible(true);

	}

}
