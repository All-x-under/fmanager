package ua.javaF1;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Track extends JPanel implements ActionListener {

	Timer mainTimer = new Timer(20, this);

	Image img = new ImageIcon("res/track.png").getImage();

	Player p = new Player();

	Motor m = new Motor();

	Bolide bolide = new Bolide();
	
	
	public Track() {
		mainTimer.start();
		
	}

	public void paint(Graphics g) {
		g = (Graphics2D) g;
		g.drawImage(img, m.layer1, 0, null);
		g.drawImage(img, m.layer2, 0, null);
		g.drawImage(p.imgMercedes, p.xMercedes, p.yMercedes, null);
		g.drawImage(p.imgFerrari, p.xFerrari, p.yFerrari, null);
		g.drawImage(p.imgRedbull, p.xRedbull, p.yRedbull, null);

	}

	public void actionPerformed(ActionEvent e) {
		m.move();
		repaint();

	}

}
